void serial_setup(){
  // Initializes Serial (USB) debugging
  Serial.begin(115200);
  delay(100);
  Serial.println("");
  Serial.println("");
  Serial.println("Pan Tilt controller, Maxime MOREILLON");
  Serial.println("");
}

String get_device_name(){
  String chip_id = String(ESP.getChipId(), HEX);
  return String(DEVICE_TYPE) + "-" + chip_id;
}

String get_device_nickname(){
  return read_string_from_eeprom(EEPROM_DEVICE_NICKNAME_ADDRESS);
}
