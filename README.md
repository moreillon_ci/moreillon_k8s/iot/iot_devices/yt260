# Pan-tilt platform firmware

Features

* MQTT
* Updates via web page
* WiFi configuration via web page
* WiFi access point if WiFi connection fails
