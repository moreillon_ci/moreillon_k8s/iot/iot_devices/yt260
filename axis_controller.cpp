#include <Arduino.h>

// An axis controller class to help deal with axis motion
class AxisController {

  private:

    int pin_pos; // IO pin used to move in a positive direction
    int pin_neg; // IO pin used to move in a negative direction

    boolean moving; // Flag to check if the motor is actualing rotating

    long stop_time = 0; // Time at which the motor should stop moving

    const int increment_duration = 500; // How long the motor should turn for each increment

  public:

    // Class constructor
    AxisController(int pin_pos, int pin_neg) {
      // Register what IO pins to use
      this->pin_pos = pin_pos;
      this->pin_neg = pin_neg;
    }

    void init(){
      // Initialize the IO pins
      pinMode(this->pin_pos,OUTPUT);
      pinMode(this->pin_neg,OUTPUT);
    }

    void move(int direction) {
      Serial.println("[Motion] Starting axis");
      // WARNING: DO not set both pins high at the same time!

      // Turning all pins LOW just to be sure
      digitalWrite(this->pin_pos,LOW);
      digitalWrite(this->pin_neg,LOW);

      // Turning the right pin HIGH
      if(direction == 1) digitalWrite(this->pin_pos,HIGH);
      else if(direction == -1) digitalWrite(this->pin_neg,HIGH);

      // Rise moving flag
      this->moving = true;

      // Set time at which motion should stop
      this->stop_time = millis() + increment_duration;
    }

    void handle() {
      // main control loop for the axis

      long now = millis();

      if(now > this->stop_time && this->moving){
        // If still moving after the motion stop time

        Serial.println("[Motion] Stopped axis");

        // Turning all pins low once finished moving
        digitalWrite(this->pin_pos,LOW);
        digitalWrite(this->pin_neg,LOW);

        // Lower flag
        this->moving = false;

      }
    }
};
